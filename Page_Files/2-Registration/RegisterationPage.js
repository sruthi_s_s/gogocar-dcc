var DataProvider = require('../../Common/excelReader.js');
var using = require('../../node_modules/jasmine-data-provider');
var RegPage =  function(){

	// STEP 1 Page Elements
	var SignUpButton = element(by.buttonText('Sign up'));
	var FullName = element(by.name('fullname'));
	var CompName = element(by.name('dealership'));
	var EmailId = element(by.name('email'));
	var PassWord = element(by.name('password'));
	var ConfirmPassWord = element(by.name('confirm_password'));
	var PhoneNumber = element(by.xpath('//*[@id="step1"]/div[6]/p-inputmask/input'));
	var UserType = element(by.name('userType'));
	var JobTitle = element(by.name('jobtitle'));
	var NextButton = element(by.xpath('/html/body/ngb-modal-window/div/div/app-registration/form/div[2]/button'));

	//STEP 2 Page Elements
	var DealerName = element(by.name('dealername'));
	var DealerAddress = element(by.name('dealeradress'));
	var WebSite = element(by.name('website'));
	var ChooseMakes = element(by.className('ui-multiselect-label-container'));
	var BackButton = element(by.buttonText('Back'));
	var SubmitButton = element(by.buttonText('Sign Up'));

	//Function to Load URL
	this.GetUrl = function(){
		browser.get('https://wizard.qa2.gogocar.com/home');
		browser.sleep(6000);
		//var SignUpButton;
		/*element(by.buttonText('Sign up')).getText().then(function(txt){
			console.log("Page" + txt);
			return txt;
		});*/
		//console.log(SignUpButton.getText());
		//return SignUpButton.getText();
		//console.log(SignUpButton);
		//return SignUpButton;
	};

	//Function to Register
	this.DealerRegistration = function(){
		var dataProvider = {
			"Case 1: Dealer registration" : {fullName : DataProvider.Reader("SignUp","B2"),compName : DataProvider.Reader("SignUp","B3"),
			emailId : DataProvider.Reader("SignUp","B4"), passWord : DataProvider.Reader("SignUp","B5"),
			confirmPassWord : DataProvider.Reader("SignUp","B5"), phoneNumber : DataProvider.Reader("SignUp","B6"),
			userType : DataProvider.Reader("SignUp","B7"), jobTitle : DataProvider.Reader("SignUp","B8"),
			dealerName : DataProvider.Reader("SignUp","B11"), dealerAddress : DataProvider.Reader("SignUp","B12"),
		    webSite : DataProvider.Reader("SignUp","B13"), chooseMakes : DataProvider.Reader("SignUp","B14")},
		};
		using(dataProvider, function(Parameter, description){
			SignUpButton.click();
			var until = protractor.ExpectedConditions;
			browser.wait(until.presenceOf(FullName),5000);
			FullName.sendKeys(Parameter.fullName);
			CompName.sendKeys(Parameter.compName);
			EmailId.sendKeys(Parameter.emailId);
			PassWord.sendKeys(Parameter.passWord);
			ConfirmPassWord.sendKeys(Parameter.confirmPassWord);
			PhoneNumber.sendKeys(Parameter.phoneNumber);
			

			UserType.click();
			if(Parameter.userType == "Dealer")
			{
				element(by.xpath('//option[contains(text(),"Dealer")]')).click();
			}
			else if (Parameter.userType == "Website Provider")
			{
				element(by.xpath('//option[contains(text(),"Website Provider")]')).click();
			}
			else
			{
				element(by.xpath('//option[contains(text(),"Dealer")]')).click();
			}
			
			JobTitle.click();
			switch(Parameter.jobTitle)
			{
				case "Sales Manager" : element(by.xpath('//option[contains(text(),"Sales Manager")]')).click();
				break;

				case "General Manager" : element(by.xpath('//option[contains(text(),"General Manager")]')).click();
				break;

				case "Owner" : element(by.xpath('//option[contains(text(),"Owner")]')).click();
				break;

				case "vendor" : element(by.xpath('//option[contains(text(),"Vendor")]')).click();
				break;

				default : element(by.xpath('//option[contains(text(),"Others")]')).click();
				break;
			}

			browser.controlFlow().execute(function(){
				browser.executeScript('arguments[0].scrollIntoView(true)',NextButton.getWebElement());
				browser.sleep(3000);
				NextButton.click();
			});
			browser.sleep(3000);
			var until1 = protractor.ExpectedConditions;
			browser.wait(until1.presenceOf(DealerName),3000);
			DealerName.sendKeys(Parameter.dealerName);
			AdressAutoComplete(Parameter.dealerAddress,1);
			browser.sleep(500);
			WebSite.sendKeys(Parameter.webSite);
			
			ChooseMakes.click();			
				element.all(by.css('.ui-multiselect-item.ui-corner-all')).all(by.tagName('label')).then(function(elements){
					//console.log(elements.length);
					for (let j=0; j<elements.length; j++) {
						var count = -1;
						elements[j].getText().then(function(txt){
							
							var makes = Parameter.chooseMakes.split(',');
						for (var i =0; i< makes.length; i++) {
							count++;
							var make = makes[i];
						//console.log("Dealership nanme=="+txt);
						//console.log("Dealership nanmefrom excel=="+make);
							if(make === txt)
							{
								//console.log("count==="+count);
								//console.log("match with excel data========"+make+ ","+ txt);
								//console.log("j value==="+j);
								elements[j].click();
								browser.sleep(500);
							}
						}
					});			
				
				}			
			  });	

			SubmitButton.click();
			browser.sleep(3000);
			});
		/*var WelcomeModalText = element(by.className('modal-title')).getText();
		return WelcomeModalText;*/
		};

		//Function to select the google auto dealer address
		function AdressAutoComplete(text, index){
			DealerAddress.sendKeys(text).then(function(){
				browser.sleep(500);
				for(var i=0; i<index; i++){
					browser.actions().sendKeys(protractor.Key.ARROW_DOWN).perform();
				}
				browser.sleep(500);
				browser.actions().sendKeys(protractor.Key.ENTER).perform();
			});
			browser.sleep(500);
		NextButton.click();	
		}
	};
	module.exports = RegPage;

