var DataProvider= require('../../Common/excelReader.js');
var using = require('../../node_modules/jasmine-data-provider'); 
var loginPage = function(){
	var loginButton = element(by.css('body > div.container-fluid > div > app-root > app-home > div > div > div > div > div > button:nth-child(1)'));
	var emailId = element(by.name('email'));
	var password = element(by.name('password'));
	var submitLoginButton = element(by.className('auth0-label-submit'));
	
	
	this.loginIntoDCC = function(){
		browser.sleep(5000);
		loginButton.click();
		var until = protractor.ExpectedConditions;
        browser.wait(until.visibilityOf(emailId),5000);
		var dataProvider= { "Case 1" : {mailid: DataProvider.Reader("Login","B1"), pwd: DataProvider.Reader("Login","B2")}};
		using(dataProvider,function(Parameter,description){
    	emailId.sendKeys(Parameter.mailid);
    	password.sendKeys(Parameter.pwd);
    	submitLoginButton.click();
    	browser.sleep(5000);
	});
};
};
module.exports = loginPage;

