var DataProvider = require('../../Common/excelReader.js');
var using = require('../../node_modules/jasmine-data-provider');
var path = require('path');
var remote = require('selenium-webdriver/remote');

var QuickBusinessPage = function(){

	//Basic information page elements
	var OnlineCheckBox = element(by.css('input[value="single"]'));
	var InstoreCheckBox = element(by.css('input[value="group"]'));
	var DearlershipWebsite = element(by.id('dealershipWebsite'));
	var CompanyName = element(by.id('dealershipName'));
	var GoGoDomain = element(by.id('domainname'));

	//Dealership page elements
	var DealershipName = element(by.css('.form-control.ui-inputtext.ui-widget.ui-state-default.ui-corner-all.ui-autocomplete-input'));
	var Manufacturer = element(by.css('select[formcontrolname="manufacturer"]'));
	var UpdateButton = element(by.buttonText('Update'));
	var AddNew = element(by.xpath('//span[contains(text(),"Add New")]'));
 	
 	//Wesiteinfo page elements
	var UploadLogo = element(by.css('input[type="file"]'));

	var PublishButton = element(by.buttonText('PUBLISH'));
	var PaylaterButton = element(by.buttonText('Pay Later'));

	var dataProvider = {
		"Quick" : {onlineCheckBox : DataProvider.Reader("BusinessProfile","B2"), instoreCheckBox : DataProvider.Reader("BusinessProfile","C2"),
	     dealershipWebsiteOnline : DataProvider.Reader("BusinessProfile","B4"), dealershipWebsiteInstore : DataProvider.Reader("BusinessProfile","C4"),
	     companyNameOnline : DataProvider.Reader("BusinessProfile","B3"), companyNameInstore : DataProvider.Reader("BusinessProfile","C3"),
	     domainOnline : DataProvider.Reader("BusinessProfile","B6"), domainInstore : DataProvider.Reader("BusinessProfile","C6"),
	     dealershipNameOnline : DataProvider.Reader("BusinessProfile","B7"), dealershipNameInstore : DataProvider.Reader("BusinessProfile","C7"),
	     manufacturerOnline : DataProvider.Reader("BusinessProfile","B8"), manufacturerInstore : DataProvider.Reader("BusinessProfile","C8"),
	     logoPath : DataProvider.Reader("BusinessProfile","B13")},
	    };

	
	//Function which calls other functions for quick online website creation
	this.QuickOnline = function(){
		
		using(dataProvider,function(Parameter,description){
			BasicInformation();
			OnlineDealership();
			WebsiteInfo();
				function BasicInformation(){
				DearlershipWebsite.sendKeys(Parameter.dealershipWebsiteOnline);
				GoGoDomain.sendKeys(Parameter.domainOnline);
			}

				function OnlineDealership(){			
				DealershipName.sendKeys(Parameter.dealershipNameOnline);
				browser.sleep(3000);
				element.all(by.css('.ui-autocomplete-list-item.ui-corner-all')).first().click();
				browser.sleep(3000);
				var make = Parameter.manufacturerOnline;
				console.log(make);
				Manufacturer.click();
				element.all(by.css('select[formcontrolname="manufacturer"]')).all(by.tagName('option')).then(function(makes){
					for(let i =0; i<makes.length; i++){
						makes[i].getText().then(function(txt){
							if(make == txt)
							{
								makes[i].click();
								browser.sleep(500);
							}

						});
					}

				});	
			}			
			
			 	function WebsiteInfo(){
			 	var AbsolutePath = path.resolve(__dirname,Parameter.logoPath);
				browser.setFileDetector(new remote.FileDetector());
				UploadLogo.sendKeys(AbsolutePath);
				browser.sleep(5000);
				PaylaterButton.click();
				browser.sleep(2000);
			}
	});
};

	//Function which calls other functions for quick instore website creation
	this.QuickInstore = function(){

		using(dataProvider,function(Parameter, description){

			BasicInfo();
			InstoreDealership();
			WebsiteInformation();
			function BasicInfo(){
				InstoreCheckBox.click();
				DearlershipWebsite.sendKeys(Parameter.dealershipWebsiteInstore);
				GoGoDomain.sendKeys(Parameter.domainInstore);
			}

			function InstoreDealership(){
				var dealers = Parameter.dealershipNameInstore.split(',');
				var brands = Parameter.manufacturerInstore.split(',');
				DealershipName.sendKeys(dealers[0]);
				browser.sleep(2000);
				element.all(by.css('.ui-autocomplete-list-item.ui-corner-all')).first().click();
				browser.sleep(2000);
				Manufacturer.sendKeys(brands[0]);
				for(let i =1;i < dealers.length; i++)
				{	
					browser.manage().window().setSize(1000,1000);
					browser.actions().mouseMove(GoGoDomain).perform();
					AddNew.click();		
					//browser.sleep(1000);		
					element.all(by.css('.form-control.ui-inputtext.ui-widget.ui-state-default.ui-corner-all.ui-autocomplete-input')).then(function(elements){
						var dealer = dealers[i];
					    //console.log(dealer);					
						browser.sleep(2000);
						elements[i].sendKeys(dealer);
						browser.sleep(2000);
						element.all(by.css('.ui-autocomplete-list-item.ui-corner-all')).first().click();
						browser.sleep(2000);
						element.all(by.css('select[formcontrolname="manufacturer"]')).then(function(makes){
							var brand = brands[i];
							makes[i].sendKeys(brand);
						});					
					});
					browser.sleep(3000);
				}				
			}

			function WebsiteInformation(){
				var AbsolutePath = path.resolve(__dirname,Parameter.logoPath);
			  	browser.setFileDetector(new remote.FileDetector());
				UploadLogo.sendKeys(AbsolutePath);
				browser.sleep(3000);
				PaylaterButton.click();
				browser.sleep(2000);
			}

		});

	};
};

module.exports = QuickBusinessPage;