var DataProvider = require('../../Common/excelReader.js');
var using = require('../../node_modules/jasmine-data-provider');

var SiteDesign = function(){
	//Select inventory elements
	var SelectInventory = element(by.xpath('//span[contains(text(),"Select inventory")]'));
	var NewCars = element(by.xpath('//span[contains(text(),"New cars")]'));
	var PreownedCars = element(by.xpath('//span[contains(text(),"Pre owned")]'));
	
	//Universal seacrh 
	var UniversalSearch = element(by.css('[data-target="#universalSearch"]')).element(by.className('slider round'));
	
	//Build your car elements
	var BuildYourCarSlider = element(by.css('[data-target="#carConfigurator"]')).element(by.className('slider round'));
	var BuildYourCar = element(by.xpath('//span[contains(text(),"Build Your Car")]'));
	var IKnowWhatIWant = element(by.xpath('//span[contains(text(),"I Know What I Want")]'));
	var IHaveNoClue = element(by.xpath('//span[contains(text(),"I Have No Clue")]'));
	var IHaveABudget = element(by.xpath('//span[contains(text(),"I Have A Budget")]'));
	var SimilarCars = element(by.id('carConfigurator')).element(by.xpath('//span[contains(text(),"Similar Cars")]'));
	var CompareCars = element(by.id('carConfigurator')).element(by.xpath('//span[contains(text(),"Compare Cars")]'));

	//Vehicle info elements	
	var VehicleInfo = element(by.xpath('//span[contains(text(),"Vehicle info")]'));
	var ColorSelection = element(by.xpath('//span[contains(text(),"Color Selection")]'));
	var TrimSelection = element(by.xpath('//span[contains(text(),"Trim Selection")]'));
	var Photos = element(by.xpath('//span[contains(text(),"Photos")]'));
	var SimilarCarsInfo = element(by.id('vehicleinfo')).element(by.xpath('//span[contains(text(),"Similar Cars")]'));
	var CompareCarsInfo = element(by.id('vehicleinfo')).element(by.xpath('//span[contains(text(),"Compare Cars")]'));
	var Rebate = element(by.xpath('//span[contains(text(),"Rebate")]'));

	//Tradein elements
	var TradeinSlider = element(by.css('[data-target="#tradein"]')).element(by.className('slider round'));
	var TradeIn = element(by.xpath('//span[contains(text(),"Trade In")]'));
	var BallParkEstimate = element(by.xpath('//span[contains(text(),"BallPark Estimate")]'));
	var AddFromRealGarage = element(by.xpath('//span[contains(text(),"Add from Real Garage")]'));
	var GogocarBuyerNetwork = element(by.xpath('//span[contains(text(),"Gogocar Buyer Network")]'));
	var UploadSingle = element(by.xpath('//span[contains(text(),"Image upload via single file")]'));
	var UploadMultiple = element(by.xpath('//span[contains(text()," Image upload via multi file")]'));
	var Auction = element(by.xpath('//span[contains(text(),"Auction")]'));

	//Finance elements
	var Finance = element(by.xpath('//span[contains(text(),"Finance")]'));
	var NewDeals = element(by.xpath('//span[contains(text(),"New Deals")]'));
	var SoftCreditPull =element(by.xpath('//span[contains(text(),"Soft credit pull")]'));
	var FinanceCalculator = element(by.xpath('//span[contains(text(),"Finance calculator")]'));
	var CreditApplication = element(by.xpath('//span[contains(text(),"Credit application")]'));

	//After market elements
	var AfterMarketSlider = element(by.css('[data-target="#aftermarket"]')).element(by.className('slider round'));
	var AfterMarket = element(by.xpath('//span[contains(text(),"After Market")]'));
	var AfterMarketItems = element(by.xpath('//span[contains(text(),"Aftermarket items")]'));
	var OptionQuestionnaire = element(by.xpath('//span[contains(text(),"Option Questionnaire")]'));

	//Final verification elements
	var FinalVerification = element(by.xpath('//span[contains(text(),"Final Verification")]'));
	var ServiceAppointment = element(by.xpath('//span[contains(text(),"Service Appointment")]'));
	var Appointment = element(by.xpath('//span[contains(text(),"Appointment")]'));

	//Dealer specif bot elements
	var DealerBotSlider = element(by.xpath('/html/body/div/div/app-root/app-dashboard/div/app-site-design/div/div[1]/div/div[2]/div[9]/div[1]/div/label/div'));
	var DealerSpecificBot = element(by.xpath('//span[contains(text(),"Dealer Specific Bot")]'));
	var BotName = element(by.name('botname'));

	//Guest Login elements
	var GuestLogin = element(by.xpath('/html/body/div/div/app-root/app-dashboard/div/app-site-design/div/div[1]/div/div[2]/div[10]/div[1]/div/label/div'));

	//Social Media links
	var SocialMediaLinks = element(by.xpath('//span[contains(text(),"Social Media Links")]'));
	var FacebookLink = element(by.name('facebook'));
	var TwitterLink = element(by.name('twitter'));
	var LinkedinLink = element(by.name('Linkdin'));
	var GoogleplusLink = element(by.name('Googleplus'));

	//Next Button
	var NextButton = element(by.buttonText('Next'));
	var SiteAnalytics = element(by.xpath('//p[contains(text(),"Site Analytics")]'));

	function ClickAction(cellvalue,element)
	{
		if(cellvalue == "OFF")
		{
			element.click();
		}
		else
		{
			return;
		}
	}

	this.SiteDesignOnline = function(){
		var dataProvider = {
			"SITEDESIGNONLINE" :{newCar : DataProvider.Reader("SiteDesign","B3"), preOwnedCar : DataProvider.Reader("SiteDesign","B4"),
			colorSelection : DataProvider.Reader("SiteDesign","B13"), trimSelection : DataProvider.Reader("SiteDesign","B14"),
			photos : DataProvider.Reader("SiteDesign","B15"), similarCars : DataProvider.Reader("SiteDesign","B16"),
			compareCars : DataProvider.Reader("SiteDesign","B17"), rebate : DataProvider.Reader("SiteDesign","B18"),
			tradeinSlider : DataProvider.Reader("SiteDesign","B19"), ballParkEstimate : DataProvider.Reader("SiteDesign","B20"),
			addFromRealGarage : DataProvider.Reader("SiteDesign","B21"), gogocarBuyerNetwork : DataProvider.Reader("SiteDesign","B22"),
			uploadSingle : DataProvider.Reader("SiteDesign","B23"), uploadMultiple : DataProvider.Reader("SiteDesign","B24"),
			auction : DataProvider.Reader("SiteDesign","B25"), finance : DataProvider.Reader("SiteDesign","B26"),
			newDeals : DataProvider.Reader("SiteDesign","B27"), softCreditPull : DataProvider.Reader("SiteDesign","B28"),
			financeCaluculator : DataProvider.Reader("SiteDesign","B29"), creditApplication : DataProvider.Reader("SiteDesign","B30"),
			afterMarketSlider : DataProvider.Reader("SiteDesign","B31"), afterMarketItems : DataProvider.Reader("SiteDesign","B32"),
			optionQuestionnaire : DataProvider.Reader("SiteDesign","B33"), serviceAppointment : DataProvider.Reader("SiteDesign","B35"),
			appointment : DataProvider.Reader("SiteDesign","B36"), dealerBotSlider : DataProvider.Reader("SiteDesign","B37"),
			botName : DataProvider.Reader("SiteDesign","B38"), guestLogin : DataProvider.Reader("SiteDesign","B39"),	
			facebookLink : DataProvider.Reader("SiteDesign","B41"), twitterLink : DataProvider.Reader("SiteDesign","B42"),
			linkedinLink : DataProvider.Reader("SiteDesign","B43"), googleplusLink : DataProvider.Reader("SiteDesign","B44")
			},
		};
		using (dataProvider,function(Parameter,description){
			SelectInventory.click();
			browser.sleep(2000);
			ClickAction(Parameter.newCar,NewCars);
			ClickAction(Parameter.preOwnedCar,PreownedCars);

			VehicleInfo.click();
			browser.sleep(2000);
			ClickAction(Parameter.colorSelection,ColorSelection);
			ClickAction(Parameter.trimSelection,TrimSelection);
			ClickAction(Parameter.photos,Photos);
			ClickAction(Parameter.similarCars,SimilarCarsInfo);
			ClickAction(Parameter.compareCars,CompareCarsInfo);
			ClickAction(Parameter.rebate,Rebate);

			ClickAction(Parameter.tradeinSlider,TradeinSlider);
			if(Parameter.tradeinSlider == "ON")
			{
				TradeIn.click();
				browser.sleep(2000);
				ClickAction(Parameter.ballParkEstimate,BallParkEstimate);
				ClickAction(Parameter.addFromRealGarage,AddFromRealGarage);
				ClickAction(Parameter.gogocarBuyerNetwork,GogocarBuyerNetwork);
				ClickAction(Parameter.uploadSingle,UploadSingle);
				ClickAction(Parameter.uploadMultiple,UploadMultiple);
				ClickAction(Parameter.auction,Auction);
			}

			Finance.click();
			browser.sleep(2000);
			ClickAction(Parameter.newDeals,NewDeals);
			ClickAction(Parameter.softCreditPull,SoftCreditPull);
			ClickAction(Parameter.financeCaluculator,FinanceCalculator);
			ClickAction(Parameter.creditApplication,CreditApplication);

			ClickAction(Parameter.afterMarketSlider,AfterMarketSlider);
			if(Parameter.afterMarketSlider == "ON")
			{
				AfterMarket.click();
				browser.sleep(2000);
				ClickAction(Parameter.afterMarketItems,AfterMarketItems);
				ClickAction(Parameter.optionQuestionnaire,OptionQuestionnaire);
			}

			FinalVerification.click();
			browser.sleep(2000);
			ClickAction(Parameter.serviceAppointment,ServiceAppointment);
			ClickAction(Parameter.appointment,Appointment);

			ClickAction(Parameter.dealerBotSlider,DealerBotSlider);
			if(Parameter.dealerBotSlider == "ON")
			{
				DealerSpecificBot.click();
				browser.sleep(2000);
				BotName.sendKeys(Parameter.botName);
			}

			ClickAction(Parameter.guestLogin,GuestLogin);

			SocialMediaLinks.click();
			browser.sleep(2000);
			FacebookLink.sendKeys(Parameter.facebookLink);
			TwitterLink.sendKeys(Parameter.twitterLink);
			LinkedinLink.sendKeys(Parameter.linkedinLink);
			GoogleplusLink.sendKeys(Parameter.googleplusLink);
			
			browser.actions().mouseMove(SiteAnalytics).perform();
			browser.sleep(2000);
			NextButton.click();
			browser.sleep(3000);
		});


	}; 

	this.SiteDesignInstore = function(){
		var dataProvider = {
			"SITEDESIGINSTORE" :{newCar : DataProvider.Reader("SiteDesign","C3"), preOwnedCar : DataProvider.Reader("SiteDesign","C4"),
			universalSearch : DataProvider.Reader("SiteDesign","C5"), buildYourCarSlider : DataProvider.Reader("SiteDesign","C6"),
			iKnowWhatIWant : DataProvider.Reader("SiteDesign","C7"), iHaveNoClue : DataProvider.Reader("SiteDesign","C8"),
			iHaveABudget : DataProvider.Reader("SiteDesign","C9"), similarCarsBuild : DataProvider.Reader("SiteDesign","C10"),
			compareCarsBuild : DataProvider.Reader("SiteDesign","C11"),
			colorSelection : DataProvider.Reader("SiteDesign","C13"), trimSelection : DataProvider.Reader("SiteDesign","C14"),
			photos : DataProvider.Reader("SiteDesign","C15"), similarCars : DataProvider.Reader("SiteDesign","C16"),
			compareCars : DataProvider.Reader("SiteDesign","C17"), rebate : DataProvider.Reader("SiteDesign","C18"),
			tradeinSlider : DataProvider.Reader("SiteDesign","C19"), ballParkEstimate : DataProvider.Reader("SiteDesign","C20"),
			addFromRealGarage : DataProvider.Reader("SiteDesign","C21"), gogocarBuyerNetwork : DataProvider.Reader("SiteDesign","C22"),
			uploadSingle : DataProvider.Reader("SiteDesign","C23"), uploadMultiple : DataProvider.Reader("SiteDesign","C24"),
			auction : DataProvider.Reader("SiteDesign","C25"), finance : DataProvider.Reader("SiteDesign","C26"),
			newDeals : DataProvider.Reader("SiteDesign","C27"), softCreditPull : DataProvider.Reader("SiteDesign","C28"),
			financeCaluculator : DataProvider.Reader("SiteDesign","C29"), creditApplication : DataProvider.Reader("SiteDesign","C30"),
			afterMarketSlider : DataProvider.Reader("SiteDesign","C31"), afterMarketItems : DataProvider.Reader("SiteDesign","C32"),
			optionQuestionnaire : DataProvider.Reader("SiteDesign","C33"), serviceAppointment : DataProvider.Reader("SiteDesign","C35"),
			appointment : DataProvider.Reader("SiteDesign","C36"), dealerBotSlider : DataProvider.Reader("SiteDesign","C37"),
			botName : DataProvider.Reader("SiteDesign","C38"), guestLogin : DataProvider.Reader("SiteDesign","C39"),	
			facebookLink : DataProvider.Reader("SiteDesign","C41"), twitterLink : DataProvider.Reader("SiteDesign","C42"),
			linkedinLink : DataProvider.Reader("SiteDesign","C43"), googleplusLink : DataProvider.Reader("SiteDesign","C44")
			},
		};
		using (dataProvider,function(Parameter,description){
		SelectInventory.click();
			browser.sleep(2000);
			ClickAction(Parameter.newCar,NewCars);
			ClickAction(Parameter.preOwnedCar,PreownedCars);

			ClickAction(Parameter.universalSearch,UniversalSearch);

			ClickAction(Parameter.buildYourCarSlider,BuildYourCarSlider);
			if(Parameter.buildYourCarSlider == "ON")
			{
				BuildYourCar.click();
				browser.sleep(2000);
				ClickAction(Parameter.iKnowWhatIWant,IKnowWhatIWant);
				ClickAction(Parameter.iHaveNoClue,IHaveNoClue);
				ClickAction(Parameter.iHaveABudget,IHaveABudget);
				ClickAction(Parameter.similarCarsBuild,SimilarCars);
				ClickAction(Parameter.compareCarsBuild,CompareCars);
			}

			VehicleInfo.click();
			browser.sleep(2000);
			ClickAction(Parameter.colorSelection,ColorSelection);
			ClickAction(Parameter.trimSelection,TrimSelection);
			ClickAction(Parameter.photos,Photos);
			ClickAction(Parameter.similarCars,SimilarCarsInfo);
			ClickAction(Parameter.compareCars,CompareCarsInfo);
			ClickAction(Parameter.rebate,Rebate);

			ClickAction(Parameter.tradeinSlider,TradeinSlider);
			if(Parameter.tradeinSlider == "ON")
			{
				TradeIn.click();
				browser.sleep(2000);
				ClickAction(Parameter.ballParkEstimate,BallParkEstimate);
				ClickAction(Parameter.addFromRealGarage,AddFromRealGarage);
				ClickAction(Parameter.gogocarBuyerNetwork,GogocarBuyerNetwork);
				ClickAction(Parameter.uploadSingle,UploadSingle);
				ClickAction(Parameter.uploadMultiple,UploadMultiple);
				ClickAction(Parameter.auction,Auction);
			}

			Finance.click();
			browser.sleep(2000);
			ClickAction(Parameter.newDeals,NewDeals);
			ClickAction(Parameter.softCreditPull,SoftCreditPull);
			ClickAction(Parameter.financeCaluculator,FinanceCalculator);
			ClickAction(Parameter.creditApplication,CreditApplication);

			ClickAction(Parameter.afterMarketSlider,AfterMarketSlider);
			if(Parameter.afterMarketSlider == "ON")
			{
				AfterMarket.click();
				browser.sleep(2000);
				ClickAction(Parameter.afterMarketItems,AfterMarketItems);
				ClickAction(Parameter.optionQuestionnaire,OptionQuestionnaire);
			}

			FinalVerification.click();
			browser.sleep(2000);
			ClickAction(Parameter.serviceAppointment,ServiceAppointment);
			ClickAction(Parameter.appointment,Appointment);

			ClickAction(Parameter.dealerBotSlider,DealerBotSlider);
			if(Parameter.dealerBotSlider == "ON")
			{
				DealerSpecificBot.click();
				browser.sleep(2000);
				BotName.sendKeys(Parameter.botName);
			}

			ClickAction(Parameter.guestLogin,GuestLogin);

			SocialMediaLinks.click();
			browser.sleep(2000);
			FacebookLink.sendKeys(Parameter.facebookLink);
			TwitterLink.sendKeys(Parameter.twitterLink);
			LinkedinLink.sendKeys(Parameter.linkedinLink);
			GoogleplusLink.sendKeys(Parameter.googleplusLink);
			
			browser.actions().mouseMove(SiteAnalytics).perform();
			NextButton.click();
			
		});

	};

}
module.exports = SiteDesign;