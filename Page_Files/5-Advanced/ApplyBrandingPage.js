var DataProvider = require('../../Common/excelReader.js');
var using = require('../../node_modules/jasmine-data-provider');
var path = require('path');
var remote = require('selenium-webdriver/remote');

var ApplyBrandingPage = function(){
	var CompanyName = element(by.name('companyname'));
	var PriceTag = element(by.name('pricetag'));
	var SavingsTag = element(by.name('savingtag'));
	var UploadLogo = element(by.css('input[type="file"]'));
	var PrimaryColor = element(by.name('pmyColor'));
	var SecondaryColor = element(by.name('scdColor'));
	var SelectFont = element(by.css('.dropdown.dropdown-select-font'));
	var AboutUs = element.all(by.buttonText('View and Edit')).first();
	var PrivacyPolicy = element.all(by.buttonText('View and Edit')).last();
	var NextButton = element(by.buttonText('Next'));
	var SiteDesign = element(by.xpath('//p[contains(text(),"Site Design")]'));

	var dataProvider = {
		"APPLYBRANDING" : {companyName : DataProvider.Reader("ApplyBranding","B2"), priceTag : DataProvider.Reader("ApplyBranding","B3"),
						  savingsTag : DataProvider.Reader("ApplyBranding","B4"), uploadLogo : DataProvider.Reader("ApplyBranding","B5"),
						  primaryColor : DataProvider.Reader("ApplyBranding","B6"), secondaryColor : DataProvider.Reader("ApplyBranding","B7"),
						  selectFont : DataProvider.Reader("ApplyBranding","B8"), aboutUs : DataProvider.Reader("ApplyBranding","B9"),
						  privacyPolicy : DataProvider.Reader("ApplyBranding","B10"),},
	};

	this.ApplyBranding = function(){

		using(dataProvider,function(Parameter,description){

			CompanyName.clear();
			CompanyName.sendKeys(Parameter.companyName);
			PriceTag.clear();
			PriceTag.sendKeys(Parameter.priceTag);
			SavingsTag.clear();
			SavingsTag.sendKeys(Parameter.savingsTag);

			var AbsolutePath = path.resolve(__dirname,Parameter.uploadLogo);
			browser.setFileDetector(new remote.FileDetector());
			UploadLogo.sendKeys(AbsolutePath);
			browser.sleep(3000);

			var font = Parameter.selectFont;
			SelectFont.click();
			element.all(by.css('.dropdown.dropdown-select-font')).all(by.tagName('a')).then(function(fonts){
				for(let i=0; i<fonts.length; i++)
				{
					fonts[i].getText().then(function(option){
						if(option === font)
						{
							fonts[i].click();
							browser.sleep(1000);
						}
					});
				}
			});
			browser.sleep(1000);
			browser.actions().mouseMove(SiteDesign).perform();
			NextButton.click();
			browser.sleep(3000);
		});
	};
}
module.exports = ApplyBrandingPage;
