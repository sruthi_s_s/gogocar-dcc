var Publish = function(){
	var NextButton = element(by.buttonText('Next'));
	var PayLaterButton = element(by.buttonText('Pay Later'));
	var PublishButton = element(by.buttonText('Publish'));
	//var SiteAnalytics = element(by.xpath('//p[contains(text(),"Site Analytics")]'));

	this.SitePublish = function(){
		browser.sleep(2000);
		NextButton.click();
		browser.sleep(2000);
		PayLaterButton.click();
		browser.sleep(2000);
		PublishButton.click();
		browser.sleep(4000);
	};

}
module.exports = Publish;