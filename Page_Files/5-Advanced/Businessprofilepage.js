var DataProvider= require('../../Common/excelReader.js');
var using = require('../../node_modules/jasmine-data-provider'); 

var businessprofile = function(){
	var instore = element(by.xpath('/html/body/div/div/app-root/app-dashboard/div/app-business-profile/div/form/div[1]/div[2]/div/div[2]/div[1]/div/label[2]/input'));
    var dealershipwebsite = element(By.id('dealershipWebsite'));
    var domainName = element(By.id('domainname'));
    var dealershipdetail = element(by.xpath("/html/body/div/div/app-root/app-dashboard/div/app-business-profile/div/form/div[1]/div[2]/div/div[3]/div/div[2]/div/div/div/div[2]/table/tbody/tr/td[1]/p-autocomplete/span/input"));
    var dealershipselection = element(by.xpath("//html/body/div/div/app-root/app-dashboard/div/app-business-profile/div/form/div[1]/div[2]/div/div[3]/div/div[2]/div/div/div/div[2]/table/tbody/tr/td[1]/p-autocomplete/span/div/ul/li"));
    var manufacturere = element(by.css('[placeholder="manufacturer"]'));
    var dataprov = element(By.id('dataProvider'));
    var tradeinprov = element(by.css('[name="tradeinprovider"]'));
    var aftermarketprov = element(by.css('[name="aftermarketprovider"]'));
    var eofferprov = element(by.css('[name="eofferprovider"]'));
    var nextButton= element(by.className('btn btn-primary btn-lg pull-right'));
    var addnewbtn = element(by.xpath('/html/body/div/div/app-root/app-dashboard/div/app-business-profile/div/form/div[1]/div[2]/div/div[3]/div/div[1]/div/span'));
    var dealershiparray= element.all(by.css('.ui-autocomplete-list-item.ui-corner-all'));
    var dealersuggestion= element.all(by.css('.form-control.ui-inputtext.ui-widget.ui-state-default.ui-corner-all.ui-autocomplete-input'));
    var manufacturearray= element.all(by.css('select[formcontrolname="manufacturer"]'));
    var SiteDesign = element(by.xpath('//p[contains(text(),"Site Design")]'));

this.businessProfileOnline = function(){
        	browser.sleep(5000);        
            var dataProvider= { "Case 1" : {
            dealerweb: DataProvider.Reader("BusinessProfile","D4"), 
            domains: DataProvider.Reader("BusinessProfile","D6"),
            dealershipname:DataProvider.Reader("BusinessProfile","D7"),
            manufactur:DataProvider.Reader("BusinessProfile","D8"),
            data1:DataProvider.Reader("BusinessProfile","D9"),
            tradein: DataProvider.Reader("BusinessProfile","D10"),
            aftermarket:DataProvider.Reader("BusinessProfile","D11"),
            eoffer:DataProvider.Reader("BusinessProfile","D12"),
            }};

            using(dataProvider,function(Parameter,description){

            	Basicinfo();
            	dealershipDetails();
            	providerInfo();

                function Basicinfo () {
                dealershipwebsite.sendKeys(Parameter.dealerweb);
                domainName.sendKeys(Parameter.domains);
             	};

 				function dealershipDetails () {
                dealershipdetail.sendKeys(Parameter.dealershipname);
                browser.sleep(2000);
                dealershipselection.click();
                browser.sleep(1000);
                manufacturere.sendKeys(Parameter.manufactur);   
            	};

            	function providerInfo () {        
                dataprov.sendKeys(Parameter.data1);
                tradeinprov.sendKeys(Parameter.tradein);
                aftermarketprov.sendKeys(Parameter.aftermarket);
                eofferprov.sendKeys(Parameter.eoffer);
                browser.sleep(5000);
            };
           
                browser.actions().mouseMove(SiteDesign).perform();       
            	browser.executeScript("arguments[0].click();", nextButton);
            	browser.sleep(6000);
            	});
        };
  this.businessProfileInstore = function(){
         browser.sleep(3000);
        
            var dataProvider= { "Case 1" : {
            dealerweb: DataProvider.Reader("BusinessProfile","E4"), 
            domains: DataProvider.Reader("BusinessProfile","E6"),
            dealershipname:DataProvider.Reader("BusinessProfile","E7"),
            manufactur:DataProvider.Reader("BusinessProfile","E8"),
            data1:DataProvider.Reader("BusinessProfile","E9"),
            tradein: DataProvider.Reader("BusinessProfile","E10"),
            aftermarket:DataProvider.Reader("BusinessProfile","E11"),
            eoffer:DataProvider.Reader("BusinessProfile","E12"),
             }};

 using(dataProvider,function(Parameter,description){

 	            instore.click();
 	            Basicinfo();
            	dealershipDetails();
            	providerInfo();
               
                
                function Basicinfo () {
                dealershipwebsite.sendKeys(Parameter.dealerweb);
                domainName.sendKeys(Parameter.domains);
             	};


               function dealershipDetails () {
               var addnew = Parameter.dealershipname.split(',');
               var manufac= Parameter.manufactur.split(',');
               dealershipdetail.sendKeys(addnew[0]);
               browser.sleep(2000);
               dealershiparray.first().click();
                browser.sleep(1000);
                manufacturere.sendKeys(manufac[0]);

                for(let i =1;i< addnew.length; i++)
				{	
					addnewbtn.click();
					browser.sleep(2000);
					dealersuggestion.then(function(elements){
						var addnews= addnew[i];
						browser.sleep(2000);
						elements[i].sendKeys(addnews);
						browser.sleep(2000);
						dealershiparray.first().click();
						browser.sleep(2000);
						manufacturearray.then(function(makes){
							var manufacs = manufac[i];
							makes[i].sendKeys(manufacs);
						});
					});
				};


            	}; 
           
           
                function providerInfo () {        
                dataprov.sendKeys(Parameter.data1);
                tradeinprov.sendKeys(Parameter.tradein);
                aftermarketprov.sendKeys(Parameter.aftermarket);
                eofferprov.sendKeys(Parameter.eoffer);
                browser.sleep(5000);
                };
                browser.actions().mouseMove(SiteDesign).perform();
                browser.executeScript("arguments[0].click();", nextButton);
            	browser.sleep(3000);
                       
                    });

  };         
};
        

	
module.exports = businessprofile;


