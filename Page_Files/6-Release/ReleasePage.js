var DataProvider = require('../../Common/excelReader.js');
var using = require('../../node_modules/jasmine-data-provider');
var ReleaseTab = function(){

	//Page elements
	var RepublishOptions = element.all(by.name('buildMethod'));
	var ReleaseTag = element(by.name('releaseTag'));
	var BranchName = element(by.name('branchName'));
	var Environment = element.all(by.name('environment'));
	var RepublishButton = element(by.buttonText('Re Publish'));
	var DeleteButton = element(by.buttonText('Delete'));
	var DomainName = element(by.css('input[placeholder="Search Domain Name"]'));
	var YesButton = element(by.xpath('//span[contains(text(),"Yes")]'));
	var NoButton = element(by.xpath('//span[contains(text(),"No")]'));

	this.RepublishAction = function(){
		var dataProvider = {
			"Case 1: Republishing" : {republishOptions : DataProvider.Reader("Release","B1"),
			releaseTag : DataProvider.Reader("Release","B2"), branchName : DataProvider.Reader("Release","B3"),
			environment : DataProvider.Reader("Release","B4"), domainName : DataProvider.Reader("Release","B5")
			},
		};
		using(dataProvider, function(Parameter, description){
			if(Parameter.republishOptions == "Branch")
			{
				RepublishOptions.first().click();
			}
			else if (Parameter.republishOptions == "Tag")
			{
				RepublishOptions.last().click();
			}
			else
			{
				RepublishOptions.first().click();
			}
			
			ReleaseTag.sendKeys(Parameter.releaseTag);
			BranchName.sendKeys(Parameter.branchName);

			switch(Parameter.environment)
			{
				case "Dev" : Environment.get(0).click();
				break;
				case "Qa2" : Environment.get(1).click();
				break;
				case "Prod" : Environment.get(2).click();
				break;
			}

			var domains = Parameter.domainName.split(',');
			for(var i=0; i<domains.length; i++)
			{
				var domain = domains[i];
				DomainName.clear().sendKeys(domain);
				browser.sleep(2000);
				element(by.css('.ui-datatable-data.ui-widget-content.ui-datatable-hoverable-rows')).click();
				browser.sleep(2000);
			}

			RepublishButton.click();
			browser.sleep(1000);
			YesButton.click();
			browser.sleep(2000);
		});

	};
}
module.exports = ReleaseTab;