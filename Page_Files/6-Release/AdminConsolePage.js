var DataProvider = require('../../Common/excelReader.js');
var using = require('../../node_modules/jasmine-data-provider');
var AdminPage = function(){

	//Page elements
	var AdminConsoleLink = element(by.xpath('//a[contains(text(),"Admin Console")]'));
	var ReleaseButton = element(by.xpath('//a[contains(text(),"Release")]'));

	this.ReleaseTabClick = function(){
		AdminConsoleLink.click();
		browser.sleep(3000);
		ReleaseButton.click();
		browser.sleep(3000);
	};
}
module.exports = AdminPage;