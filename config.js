var path = require('path');

var PrettyReporter = require('protractor-pretty-html-reporter').Reporter;

var prettyReporter = new PrettyReporter({
    // required, there is no default
    path: path.join(__dirname, 'results'),
    screenshotOnPassed: true
});

// An example configuration file.
exports.config = {
  //directConnect: true,

  // Capabilities to be passed to the webdriver instance.
    'seleniumAddress': 'http://hub-cloud.browserstack.com/wd/hub',
  'commonCapabilities': {

    'browserstack.user': 'anshadameenza2',
    'browserstack.key': 'MpQmN5o96Gr7fFdPaTYX',
    'browserstack.debug':true,
  // 'browserstack.safari.enablePopups': false,
  // 'SetCapability':{"autoDismissAlerts": true},
  // 'chromeOptions': {
    // 'args': ["--enable-popup-blocking"]
    // 'excludeSwitches': ["disable-popup-blocking"]
  // },
   
  },
  'multiCapabilities': [
  // {
  //      'platform' : 'MAC',
  //      'browserName' : 'iPhone',
  //      'device' : 'iPhone 6S Plus',
  
 /* build: '9027654bd694455c826047d4d88d062b',
     'tunnel-identifier': '9027654bd694455c826047d4d88d062b',
    platformName: 'iOS',
    platformVersion: '7.1',
    browserName: '',
    app: 'safari',
    deviceName: 'iPhone Simulator',
    'appium-version' : '1.6.0'*/
  // },
 /* {
     'platform' : 'ANDROID',
     'browserName' : 'android',
     'device' : 'Samsung Galaxy S5',
  }*/
  // ,
   {
     'browserName': 'chrome'
   }
  ],

  // Framework to use. Jasmine is recommended.
  framework: 'jasmine2',
  allScriptsTimeout: 120000,
  getPageTimeout: 120000,

  // Spec patterns are relative to the current working directly when
  // protractor is called.
  /* specs: [
           'Getting_URL.js',
         ],
  */
  suites:{
    Login :'Suites/1-Login/TestLogin.js',
    Quick_Online : 'Suites/QuickOnline/spec.js',
    Quick_Instore : 'Suites/QuickInstore/spec.js',
    Advanced_Online : 'Suites/AdvancedOnline/spec.js',
    Advanced_Instore : 'Suites/AdvancedInstore/spec.js',
    Republish : 'Suites/Release/spec.js'
  },  
  // Options to be passed to Jasmine.
  
  jasmineNodeOpts: {
    //showColors: true,
    defaultTimeoutInterval: 1000000,
    //includeStackTrace : true,
    //isVerbose : true,
   // print: function () {}
},
//59iqijamH%O&oD^YfYmH
onPrepare: function() {
        jasmine.getEnv().addReporter(prettyReporter);
    }
  
};

// Code to support common capabilities
exports.config.multiCapabilities.forEach(function(caps){
  for(var i in exports.config.commonCapabilities) caps[i] = caps[i] || exports.config.commonCapabilities[i];
});
