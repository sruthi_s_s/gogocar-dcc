var ReleasePage = require('../../Page_Files/6-Release/ReleasePage.js');
var AdminPage = require('../../Page_Files/6-Release/AdminConsolePage.js');

describe('Site Republish',function(){

	//Select release tab
	it('Click Release tab',function(){
		var obj1 = new AdminPage();
		obj1.ReleaseTabClick();
	});

	//Reppublish sites
	it('Site Republish',function(){
		var obj2 = new ReleasePage();
		obj2.RepublishAction();

		//Validating success message
		var SuccessMessage = element(by.id('toast-container'));
		expect(SuccessMessage.isDisplayed()).toBe(true);
	});

});