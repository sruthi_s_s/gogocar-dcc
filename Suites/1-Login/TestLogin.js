var loginPage = require('../../Page_Files/1-Login/LoginPage.js');

var login = new loginPage();

describe('DCC LOGIN', function() {

    // Getting URL													
	it('Get Url', function(){
		//browser.get('https://wizard.qa2.gogocar.com');
        var url = process.env.URLA;
        browser.get(url);
        console.log('Automation testing on : '+url);
		browser.manage().window().maximize();
	})

    // Login into DCC 
    it('Login into DCC', function() {												
     browser.ignoreSynchronization=true;
     login.loginIntoDCC();    
     browser.sleep(5000);
     expect(browser.getCurrentUrl()).toEqual('https://wizard.qa2.gogocar.com/myhome'); // Checking DCC Login
    }); 
});