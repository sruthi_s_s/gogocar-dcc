var Homepage = require('../../Page_Files/3-Home/AdvancedHomePage.js');
var Businesspage = require('../../Page_Files/5-Advanced/Businessprofilepage.js');
var ApplyBranding  = require('../../Page_Files/5-Advanced/ApplyBrandingPage.js');
var SiteDesign = require('../../Page_Files/5-Advanced/SiteDesignPage.js');
var PublishSite = require('../../Page_Files/5-Advanced/PublishPage.js');

describe('ADVANCED ONLINE SITE CREATION',function(){
	
	//Select Advanced option
	it('SELECT ADVANCED',function(){
		browser.ignoreSynchronization=true;
		var obj1 = new Homepage();
  		obj1.advancedProvision();
	});

	//Create Advanced Instore site
	it('BUSINESS INFORMATION PAGE',function(){
		browser.ignoreSynchronization=true;
		var obj2 = new Businesspage();
		obj2.businessProfileInstore();
	});

	it('APPLY BRANDING PAGE',function(){
		var obj3 = new ApplyBranding();
		obj3.ApplyBranding();
	});

	it('SITE DESIGN PAGE',function(){
		var obj4 = new SiteDesign();
		obj4.SiteDesignInstore();
	});

	it('PUBLISH PAGE',function(){
		var obj5 = new PublishSite();
		obj5.SitePublish();

		var SuccessParagraph = element(by.css('.text-success.mark'));
    	expect(SuccessParagraph.isDisplayed()).toBe(true);
	
	});
});