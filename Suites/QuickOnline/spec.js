var Homepage = require('../../Page_Files/3-Home/QuickHomePage.js');
var QuickPage = require('../../Page_Files/4-Quick/QuickBusinessPage.js');

describe('QUICK ONLNE WEBSITE CREATION',function(){

	//Select Quick option
	it('SELECT QUICK OPTION',function(){
		var obj1 = new Homepage();
		obj1.quickProvision();

	});

	//Crete Quick Online Website
	it('CREATE QUICK ONLINE WEBSITE',function(){
		var obj2 = new QuickPage();
		obj2.QuickOnline();

        var SuccessParagraph = element(by.css('.text-success.mark'));
        expect(SuccessParagraph.isDisplayed()).toBe(true);
	});
});
