var excelReader =  function(){
if(typeof require !== 'undefined')XLSX = require("../node_modules/xlsx"); // path for xlxs directory that you have downloaded via npm
var workbook = XLSX.readFile("Common/TestSheet.xlsx");
//var first_sheet_name = workbook.SheetNames[0];
this.Reader = function(sheetValue,cellValue){
    var address_of_cell = cellValue;
    var address_of_sheet = sheetValue;
    var worksheet = workbook.Sheets[address_of_sheet];
    var desired_cell = worksheet[address_of_cell];
    var desired_value = (desired_cell ? desired_cell.v : undefined);
    return desired_value;
};
};
module.exports = new excelReader();